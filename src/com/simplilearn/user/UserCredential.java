package com.simplilearn.user;

public class UserCredential {
	
//	private String username;
	UserRegistration userReg;
	private String sitename;
	private String socialMediaUsername;
	private String socialMediaPassword;
	
	public UserCredential(){
		userReg = new UserRegistration();
	}
	
	public UserCredential(String sitename, String socialMediaUsername, String socialMediaPassword){
	//	this.username = username;
		this.sitename = sitename;
		this.socialMediaUsername = socialMediaUsername;
		this.socialMediaPassword = socialMediaPassword;
	}
	
	public UserRegistration getUserRegistration(){
		
		return userReg;
	}
	
/*	public void setUsername(String username){
		this.username = username;
	}
	
	public String getUsername(){
		return username;
	}*/

	public void setSitename(String sitename){
		this.sitename = sitename;
	}
	
	public String getSitename(){
		return sitename;
	}
	
	public void setSocialMediaUsername(String socialMediaUsername){
		this.socialMediaUsername = socialMediaUsername;
	}
	
	public String getSocialMediaUsername(){
		return socialMediaUsername;
	}
	
	public void setSocialMediaPassword(String socialMediaPassword){
		this.socialMediaPassword = socialMediaPassword;
	}
	
	public String getSocialMediaPassword(){
		return socialMediaPassword;
	}
	
	@Override
	public String toString(){
		
		return "For the SiteID: "+ sitename+" The username is: "+ socialMediaUsername + " and the password is: "+ socialMediaPassword;
	}
	
	public String toFileFormat(){
		return getUserRegistration().getUsername() +  ":" + this.getSitename() + ":"
				+ this.getSocialMediaUsername() + ":" + this.getSocialMediaPassword();
	}
}
